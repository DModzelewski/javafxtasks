package Main;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.shape.*;
import javafx.scene.text.Text;

public class MainClass extends Application {
    public static void main(String[]args){
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Group root = new Group();
        Polygon polygon = new Polygon(600,300,500,127,
                500,127,300,127,
                300,127,200,300,
                200,300,300,473,
                300,473,500,473,
                500,473,600,300);
        Text text = new Text("Hello in JavaFX");
        polygon.setFill(Color.RED);
        text.setFill(Color.GREEN);
        text.setX(350);
        text.setY(100);
        Circle[] circles = Methods.doCircles(50,800,600);
        Rectangle[] squaresX1 = Methods.createSquaresX(800,50, 25);
        Rectangle[] squaresX2 = Methods.createSquaresX(800,50, 525);
        Rectangle[] squaresY1 = Methods.createSquaresY(600,50, 25);
        Rectangle[] squaresY2 = Methods.createSquaresY(600,50, 725);
        root.getChildren().add(polygon);
        root.getChildren().add(text);
        for (Circle c : circles){
            root.getChildren().add(c);
        }
        for (Rectangle r : squaresX1){
            root.getChildren().add(r);
        }
        for (Rectangle r : squaresX2){
            root.getChildren().add(r);
        }
        for (Rectangle r : squaresY1){
            root.getChildren().add(r);
        }
        for (Rectangle r : squaresY2){
            root.getChildren().add(r);
        }
        Scene scene = new Scene(root, 800, 600,  Color.WHITE);
        stage.setScene(scene);
        stage.show();
    }

}
