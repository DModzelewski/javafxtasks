package Main;

import javafx.scene.paint.Color;
import javafx.scene.shape.*;

import java.util.Random;

public class Methods {
    public static Circle[] doCircles(double radius, double xRes, double yRes){
        Circle[] circles = new Circle[4];


        for (int i=0; i<4; i++){
            circles[i]=new Circle(radius);
            switch (i){
                case 0:{
                    circles[i].setCenterX(radius);
                    circles[i].setCenterY(radius);
                    break;
                }
                case 1:{
                    circles[i].setCenterX(xRes-radius);
                    circles[i].setCenterY(radius);
                    break;
                }
                case 2:{
                    circles[i].setCenterX(radius);
                    circles[i].setCenterY(yRes-radius);
                    break;
                }
                case 3:{
                    circles[i].setCenterX(xRes-radius);
                    circles[i].setCenterY(yRes-radius);
                    break;
                }
            }
            circles[i].setFill(Color.BLUE);
        }

        return circles;
    }

    public static Rectangle[] createSquaresX(double xRes, double sqrSide, double squaresHeight){
        Random random = new Random();
        double xSquaresNumber = xRes/sqrSide - 4;
        Rectangle[] squaresX = new Rectangle[(int)xSquaresNumber];
        double shift = 0;
        for (int i=0; i<xSquaresNumber; i++){
            squaresX[i] = new Rectangle(sqrSide,sqrSide);
            squaresX[i].setFill(Color.color(random.nextDouble(),random.nextDouble(),random.nextDouble()));
            squaresX[i].setX(sqrSide+sqrSide+shift);
            squaresX[i].setY(squaresHeight);
            shift+=50;
        }

        return squaresX;
    }
    public static Rectangle[] createSquaresY(double yRes, double sqrSide, double squaresWidth){
        Random random = new Random();
        double ySquaresNumber = yRes/sqrSide - 4;
        Rectangle[] squaresY = new Rectangle[(int)ySquaresNumber];
        double shift = 0;
        for (int i=0; i<ySquaresNumber; i++){
            squaresY[i] = new Rectangle(sqrSide,sqrSide);
            squaresY[i].setFill(Color.color(random.nextDouble(),random.nextDouble(),random.nextDouble()));
            squaresY[i].setY(sqrSide+sqrSide+shift);
            squaresY[i].setX(squaresWidth);
            shift+=50;
        }

        return squaresY;
    }
}
